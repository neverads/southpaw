# southpaw

by [never ads](https://play.google.com/store/apps/developer?id=Never+Ads)  

_No ads, no data-collection, no cost.  Ever.  And we're [open-source](gitlab.com/neverads)!_  

it's a simple news aggregator.  No algorithms; just the news.  
