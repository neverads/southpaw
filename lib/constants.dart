import 'package:intl/intl.dart';

const neverAdsUrl = 'gitlab.com/neverads';
const neverAdsEmail = 'never.ads.info@gmail.com';
const appTitle = 'southpaw';
const favoritesScreenTitle = 'favorites';
const infoScreenTitle = 'about this app';

const prefsKeyFavorites = 'prefsKeyFavorites';
const prefsKeySources = 'prefsKeySources';
const prefsKeyTappedArticleUrls = 'prefsKeyTappedArticleUrls';

const fontNameFutura = 'Futura';
const todayString = 'today';
const yesterdayString = 'yesterday';

final earliestDate = DateTime(1970);

const parseFormat = '${DateFormat.ABBR_WEEKDAY}, ${DateFormat.DAY} ${DateFormat.ABBR_MONTH}, ${DateFormat.YEAR}';

const sep = '___';

const favIconSize = 48.0;
