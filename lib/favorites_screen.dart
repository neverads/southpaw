import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:southpaw/constants.dart';
import 'package:southpaw/source.dart';
import 'package:southpaw/source_item_tile.dart';

import 'globals.dart';

class FavoritesScreen extends StatefulWidget {
  @override
  _FavoritesScreenState createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  @override
  Widget build(BuildContext context) {
    final List<SourceItem> favorites =
        (prefs.getStringList(prefsKeyFavorites) ?? []).map((favorite) => SourceItem.fromString(favorite)).toList();

    List<DateTime> dates = favorites.map((favorite) => favorite.date).toSet().toList();
    dates.sort((a, b) => b.compareTo(a));
    final List<Widget> children = [];
    dates.forEach((date) {
      children.add(
        Center(
          child: Padding(
            padding: EdgeInsets.only(top: dates.first == date ? 0 : 32),
            child: Builder(
              builder: (_context) {
                final formattedDate = getDateAsString(date);
                final datePrefix = [todayString, yesterdayString].contains(formattedDate) ? '' : 'on ';
                return CupertinoButton(
                  child: Text(
                    formattedDate,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.w500,
                        ),
                  ),
                  onPressed: () {
                    Scaffold.of(_context).removeCurrentSnackBar();
                    Scaffold.of(_context).showSnackBar(
                      SnackBar(
                        content: Text('The articles in this section were published $datePrefix$formattedDate.'),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ),
      );
      favorites.where((favorite) => favorite.date == date).forEach((favorite) => children.add(
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: SourceItemTile(favorite, isFavorite: true),
            ),
          ));
    });

    return Scaffold(
      appBar: AppBar(
        title: Text(favoritesScreenTitle),
      ),
      body: favorites.isEmpty
          ? emptyFavoritesMessage
          : ListView(
              padding: EdgeInsets.only(top: 8, bottom: 72),
              children: children,
            ),
    );
  }

  get emptyFavoritesMessage {
    return Padding(
      padding: EdgeInsets.all(24),
      child: Align(
        alignment: Alignment.topCenter,
        child: Text(
          'Your favorites will appear here.\n\n'
          'On the main screen, you can add an article to your favorites by tapping its ★ button.',
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ),
    );
  }
}
