import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share/share.dart';
import 'package:southpaw/source.dart';

import 'constants.dart';
import 'globals.dart';

class SourceItemTile extends StatefulWidget {
  final SourceItem item;
  final bool isFavorite;

  SourceItemTile(this.item, {this.isFavorite = false});

  @override
  _SourceItemTileState createState() => _SourceItemTileState();
}

class _SourceItemTileState extends State<SourceItemTile> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  bool areButtonsVisible = false;
  List listItems = [];

  @override
  Widget build(BuildContext context) {
    if (widget.isFavorite) {
      widget.item.isChecked = !widget.item.isFavorite;
    }

    final rowOfButtons = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        GestureDetector(
          child: IconButton(
            icon: Icon(widget.item.isFavorite ? Icons.star : Icons.star_border),
            onPressed: () => setState(() {
              if (widget.isFavorite) {
                widget.item.isChecked = !widget.item.isChecked;
              }
              return widget.item.toggleAsFavorite();
            }),
          ),
          onLongPress: () {
            Scaffold.of(context).removeCurrentSnackBar();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  children: [
                    Icon(
                      Icons.star_border,
                      size: 20,
                      color: Theme.of(context).primaryColor,
                    ),
                    SizedBox(width: 16),
                    Flexible(
                      child: Text(
                        'Tap to save this article to your "Favorites."  '
                        'Tap the favorites-button at the top of this screen to view them.',
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
        GestureDetector(
          child: IconButton(
            icon: Icon(Icons.share),
            onPressed: () async {
              rememberThatUrlWasTapped(widget.item.url);
              if (!widget.isFavorite) {
                setState(() => widget.item.isChecked = true);
              }
              await Share.share(widget.item.url, subject: widget.item.title);
            },
          ),
          onLongPress: () {
            Scaffold.of(context).removeCurrentSnackBar();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  children: [
                    Icon(
                      Icons.share,
                      size: 20,
                      color: Theme.of(context).primaryColor,
                    ),
                    SizedBox(width: 16),
                    Flexible(
                      child: Text('Tap to "Share" this article.'),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
        GestureDetector(
          child: IconButton(
            icon: Icon(Icons.open_in_browser),
            onPressed: () async {
              rememberThatUrlWasTapped(widget.item.url);
              if (!widget.isFavorite) {
                setState(() => widget.item.isChecked = true);
              }
              openWebpage(widget.item.url);
            },
          ),
          onLongPress: () {
            Scaffold.of(context).removeCurrentSnackBar();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  children: [
                    Icon(
                      Icons.open_in_browser,
                      size: 20,
                      color: Theme.of(context).primaryColor,
                    ),
                    SizedBox(width: 16),
                    Flexible(
                      child: Text('Tap to open this article in a browser.'),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
        GestureDetector(
          child: IconButton(
            icon: Icon(Icons.content_copy),
            onPressed: () {
              rememberThatUrlWasTapped(widget.item.url);
              if (!widget.isFavorite) {
                setState(() => widget.item.isChecked = true);
              }
              final maxLength = 150;
              final isUrlTooLong = widget.item.url.length > maxLength;
              final ellipsis = isUrlTooLong ? "…" : "";
              final endIndex = isUrlTooLong ? maxLength : widget.item.url.length;
              Clipboard.setData(ClipboardData(text: widget.item.url));
              Scaffold.of(context).removeCurrentSnackBar();
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('link copied:\n"${widget.item.url.substring(0, endIndex)}$ellipsis"'),
                ),
              );
            },
          ),
          onLongPress: () {
            Scaffold.of(context).removeCurrentSnackBar();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  children: [
                    Icon(
                      Icons.content_copy,
                      size: 20,
                      color: Theme.of(context).primaryColor,
                    ),
                    SizedBox(width: 16),
                    Flexible(
                      child: Text('Tap to copy the link to this article.'),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ],
    );

    if (!widget.isFavorite) {
      rowOfButtons.children.insert(
        0,
        GestureDetector(
            child: Checkbox(
              value: widget.item.isChecked,
              onChanged: (bool newValue) async {
                if (newValue) {
                  rememberThatUrlWasTapped(widget.item.url);
                } else {
                  forgetThatUrlWasTapped(widget.item.url);
                }
                setState(() => widget.item.isChecked = newValue);
              },
              activeColor: Theme.of(context).toggleableActiveColor,
              checkColor: Theme.of(context).primaryColorLight,
            ),
            onLongPress: () {
              Scaffold.of(context).removeCurrentSnackBar();
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Row(
                    children: [
                      Icon(
                        Icons.check_box,
                        size: 20,
                        color: Theme.of(context).primaryColor,
                      ),
                      Text(' / '),
                      Icon(
                        Icons.check_box_outline_blank,
                        size: 20,
                        color: Theme.of(context).primaryColor,
                      ),
                      SizedBox(width: 16),
                      Flexible(
                        child: Text('Toggle the checkbox to mark this article as read/unread.'),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );
    }

    return Card(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              children: <Widget>[
                widget.isFavorite
                    ? Image.network(
                        widget.item.imageUrl,
                        width: favIconSize,
                        height: favIconSize,
                        scale: 0.8,
                        errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                          return ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: favIconSize, maxHeight: favIconSize),
                            child: SizedBox(),
                          );
                        },
                      )
                    : SizedBox(),
                Expanded(
                  child: CupertinoButton(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                    child: AnimatedOpacity(
                      opacity: widget.item.isChecked ? 0.6 : 1.0,
                      duration: Duration(milliseconds: 300),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          widget.item.title,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ),
                    ),
                    onPressed: () {
                      if (listItems.isEmpty) {
                        _listKey.currentState.insertItem(
                          0,
                          duration: Duration(milliseconds: 150),
                        );
                        listItems.add(rowOfButtons);
                      } else {
                        _listKey.currentState.removeItem(
                          0,
                          (BuildContext context, Animation<double> animation) => _buildItem(rowOfButtons, animation),
                          duration: Duration(milliseconds: 150),
                        );
                        listItems.clear();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
          AnimatedList(
            key: _listKey,
            initialItemCount: listItems.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index, Animation<double> animation) {
              return _buildItem(rowOfButtons, animation);
            },
          ),
        ],
      ),
    );
  }

  _buildItem(Row row, Animation<double> animation) {
    return SizeTransition(
      sizeFactor: animation,
      axisAlignment: -1,
      child: FadeTransition(
        opacity: animation,
        child: row,
      ),
    );
  }

  rememberThatUrlWasTapped(String url) {
    List<String> urls = prefs.getStringList(prefsKeyTappedArticleUrls) ?? [];
    urls.add(url);
    prefs.setStringList(prefsKeyTappedArticleUrls, urls.toSet().toList());
  }

  forgetThatUrlWasTapped(String url) {
    List<String> urls = prefs.getStringList(prefsKeyTappedArticleUrls) ?? [];
    urls.remove(url);
    prefs.setStringList(prefsKeyTappedArticleUrls, urls.toSet().toList());
  }
}
