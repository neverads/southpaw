import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:southpaw/constants.dart';
import 'package:southpaw/favorites_screen.dart';
import 'package:southpaw/info_screen.dart';
import 'package:southpaw/settings_screen.dart';
import 'package:southpaw/source.dart';
import 'package:southpaw/source_item_tile.dart';
import 'package:webfeed/webfeed.dart';

import 'globals.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const dividerIndent = 24.0;

    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.blueGrey,
        primaryColorLight: Colors.blueGrey.shade50,
        primaryColorDark: Colors.blueGrey.shade900,
        accentColor: Colors.blueGrey.shade700,
        dividerColor: Colors.blueGrey.shade700,
        unselectedWidgetColor: Colors.blueGrey.shade700,
        buttonColor: Colors.blueGrey.shade700,
        toggleableActiveColor: Colors.blueGrey.shade700,
        fontFamily: fontNameFutura,
        textTheme: TextTheme(
          headline5: TextStyle(color: Colors.blueGrey.shade700, fontWeight: FontWeight.w500),
          headline6: TextStyle(color: Colors.blueGrey.shade900),
          bodyText2: TextStyle(color: Colors.blueGrey.shade900),
          subtitle1: TextStyle(color: Colors.blueGrey.shade900),
        ),
        iconTheme: IconThemeData(color: Colors.blueGrey.shade700),
        dividerTheme: DividerThemeData(indent: dividerIndent, endIndent: dividerIndent),
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blueGrey.shade700,
        primaryColorLight: Colors.blueGrey.shade900,
        primaryColorDark: Colors.blueGrey.shade50,
        accentColor: Colors.blueGrey.shade200,
        dividerColor: Colors.blueGrey.shade200,
        unselectedWidgetColor: Colors.blueGrey.shade200,
        buttonColor: Colors.blueGrey.shade200,
        toggleableActiveColor: Colors.blueGrey.shade200,
        fontFamily: fontNameFutura,
        textTheme: TextTheme(
          headline5: TextStyle(color: Colors.blueGrey.shade200, fontWeight: FontWeight.w500),
          headline6: TextStyle(color: Colors.blueGrey.shade50),
          bodyText2: TextStyle(color: Colors.blueGrey.shade50),
          subtitle1: TextStyle(color: Colors.blueGrey.shade50),
        ),
        iconTheme: IconThemeData(color: Colors.blueGrey.shade200),
        dividerTheme: DividerThemeData(indent: dividerIndent, endIndent: dividerIndent),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime date = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  final today = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  @override
  Widget build(BuildContext context) {
    clearOldRememberedUrls();
    removeOldSourceData();

    final canIncrementDate = today.difference(date).inDays > 0;
    final canDecrementDate = date.difference(earliestDate).inDays > 0;

    const popupMenuValueFavorite = 0;
    const popupMenuValueInfo = 1;

    return Scaffold(
      appBar: AppBar(
        title: Text(appTitle),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () => setState(() => sources.forEach((source) => source.reset())),
          ),
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () async {
              await Navigator.of(context).push(MaterialPageRoute(builder: (context) => SettingsScreen()));
              setState(() {});
            },
          ),
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                value: popupMenuValueFavorite,
                child: Text(favoritesScreenTitle),
              ),
              PopupMenuItem(
                value: popupMenuValueInfo,
                child: Text(infoScreenTitle),
              ),
            ],
            onSelected: (value) async {
              if (value == popupMenuValueFavorite) {
                await Navigator.of(context).push(MaterialPageRoute(builder: (context) => FavoritesScreen()));
                setState(() {});
              } else if (value == popupMenuValueInfo) {
                await Navigator.of(context).push(MaterialPageRoute(builder: (context) => InfoScreen()));
                setState(() {});
              }
            },
          ),
        ],
      ),
      body: GestureDetector(
        onHorizontalDragEnd: (DragEndDetails details) {
          if (canIncrementDate && details.primaryVelocity < 0) {
            setState(() => date = date.add(Duration(days: 1)));
          } else if (canDecrementDate && details.primaryVelocity > 0) {
            setState(() => date = date.subtract(Duration(days: 1)));
          }
        },
        child: Column(
          children: <Widget>[
            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.chevron_left),
                      onPressed:
                          canDecrementDate ? () => setState(() => date = date.subtract(Duration(days: 1))) : null,
                    ),
                    CupertinoButton(
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(minWidth: 140),
                        child: Center(
                          child: Text(
                            getDateAsString(date),
                            style: Theme.of(context).textTheme.subtitle1.copyWith(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.w500,
                                ),
                          ),
                        ),
                      ),
                      padding: EdgeInsets.symmetric(vertical: 16),
                      onPressed: () {
                        showDatePicker(
                          context: context,
                          initialDate: date,
                          firstDate: earliestDate,
                          lastDate: today,
                        ).then((selectedDate) {
                          if (selectedDate != null) {
                            setState(() => date = selectedDate);
                          }
                        });
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.chevron_right),
                      onPressed: canIncrementDate ? () => setState(() => date = date.add(Duration(days: 1))) : null,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 1.5,
              child: Container(
                decoration: BoxDecoration(color: Theme.of(context).primaryColor),
              ),
            ),
            prefs == null
                ? FutureBuilder(
                    future: SharedPreferences.getInstance(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        prefs = snapshot.data;
                        final sourcesAsStrings = prefs.getStringList(prefsKeySources) ?? [];
                        sources = sourcesAsStrings.map((sourceAsString) => Source.fromString(sourceAsString)).toList();
                        return _body;
                      } else {
                        return SizedBox();
                      }
                    },
                  )
                : _body,
          ],
        ),
      ),
    );
  }

  get _body {
    return sources.isEmpty
        ? Padding(
            padding: EdgeInsets.only(top: 32),
            child: ElevatedButton(
              onPressed: () async {
                await Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => SettingsScreen(shouldExpandAddSourceSection: true),
                  ),
                );
                setState(() {});
              },
              child: Text('Add a source'),
            ),
          )
        : Expanded(
            child: ListView.builder(
              itemCount: sources.length,
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 8, bottom: 72),
              itemBuilder: (context, index) {
                final source = sources[index];
                if (source.whenFetched == null) {
                  source.whenFetched = DateTime.now();
                  return FutureBuilder(
                    future: getSourceItems(source),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          source.error = snapshot.error;
                        } else {
                          source.error = null;
                        }
                      } else {
                        return _listTile(source, isFetching: true);
                      }
                      return _listTile(source);
                    },
                  );
                } else {
                  return _listTile(source);
                }
              },
            ),
          );
  }

  Builder _listTile(Source source, {bool isFetching = false}) {
    final GlobalKey<AnimatedListState> _listKey = GlobalKey();
    List<SourceItem> _listItems = [];
    List<SourceItem> sourceItems = source.getItemsWithDate(date);

    tryExpandingList(int delayMillis) {
      Future.delayed(Duration(milliseconds: delayMillis)).then((value) {
        try {
          if (source.shouldDisplayItems) {
            for (var i = 0; i < sourceItems.length; i++) {
              _listKey.currentState.insertItem(i);
              _listItems.add(sourceItems.elementAt(i));
            }
          }
        } catch (e) {
          if (delayMillis < 500) {
            tryExpandingList(delayMillis + 10);
          }
        }
      });
    }

    tryExpandingList(0);

    var trailingChild;
    if (source.error != null) {
      trailingChild = IconButton(
        icon: Icon(Icons.error_outline),
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Row(
                  children: <Widget>[
                    Icon(Icons.error_outline),
                    SizedBox(width: 8),
                    Text('Error fetching data'),
                  ],
                ),
                content: Text(
                  source.error is SocketException
                      ? 'Unable to fetch data from "${source.baseUrl}."\n\n'
                          'Please check your network connection.\n\n'
                          'Details for nerds:\n'
                          '${source.error}'
                      : 'Unable to parse the RSS-feed from "${source.baseUrl}."\n\n'
                          'Either there is an issue with your network connection, or they have done something strange with their feed, and we can\'t process it. 😬\n\n‍'
                          'If you email us with a screenshot of this, we\'ll try to fix it.\n'
                          '($neverAdsEmail)\n\n'
                          'Details for nerds:\n'
                          '${source.error}',
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      source.error is SocketException ? '' : 'copy email',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(color: Theme.of(context).buttonColor),
                    ),
                    onPressed: source.error is SocketException
                        ? null
                        : () {
                            Clipboard.setData(ClipboardData(text: neverAdsEmail));
                            Navigator.pop(context);
                          },
                  ),
                  FlatButton(
                    child: Text(
                      'ok',
                      style: Theme.of(context).textTheme.subtitle2.copyWith(color: Theme.of(context).buttonColor),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            },
          );
        },
      );
    } else if (isFetching) {
      trailingChild = Center(
        child: CircularProgressIndicator(strokeWidth: 2),
      );
    } else {
      trailingChild = Center(
        child: Text(
          '${sourceItems.length}',
          style: Theme.of(context).textTheme.headline6,
        ),
      );
    }
    return Builder(builder: (BuildContext context) {
      return Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 16),
            child: ListTile(
              leading: Image.network(
                source.imageUrl,
                width: favIconSize,
                height: favIconSize,
                scale: 0.8,
                frameBuilder: (BuildContext context, Widget child, int frame, bool wasSynchronouslyLoaded) {
                  if (wasSynchronouslyLoaded) {
                    return child;
                  } else {
                    return AnimatedOpacity(
                      child: child,
                      opacity: frame == null ? 0 : 1,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.easeOut,
                    );
                  }
                },
                errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                  return ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: favIconSize, maxHeight: favIconSize),
                    child: Center(
                      child: Text(
                        source.initials,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                  );
                },
              ),
              title: Text(
                source.title,
                style: Theme.of(context).textTheme.headline6,
              ),
              subtitle: Text(
                source.baseUrl,
                style: Theme.of(context).textTheme.bodyText2,
              ),
              trailing: SizedBox(
                width: 48,
                height: 48,
                child: trailingChild,
              ),
              onTap: () {
                if (sourceItems.isEmpty) {
                  Scaffold.of(context).removeCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                          '"${source.title}" has no articles/items for ${getDateAsString(date)} in their rss-feed.'),
                    ),
                  );
                } else {
                  source.shouldDisplayItems = !source.shouldDisplayItems;
                  for (var i = 0; i < sourceItems.length; i++) {
                    if (source.shouldDisplayItems) {
                      _listKey.currentState.insertItem(i);
                      _listItems.add(sourceItems.elementAt(i));
                    } else {
                      final isLastItem = i == _listItems.length - 1;
                      final itemToRemove = _listItems[0];
                      _listKey.currentState.removeItem(
                        0,
                        (BuildContext context, Animation<double> animation) =>
                            _buildItem(context, itemToRemove, animation, isLastItem),
                      );
                      _listItems.removeAt(0);
                    }
                  }
                }
              },
            ),
          ),
          AnimatedList(
            key: _listKey,
            initialItemCount: _listItems.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index, Animation<double> animation) {
              final isLastItem = index == _listItems.length - 1;
              return _buildItem(context, _listItems[index], animation, isLastItem);
            },
          ),
        ],
      );
    });
  }

  Widget _buildItem(BuildContext context, SourceItem item, Animation<double> animation, isLastItem) {
    return SizeTransition(
      sizeFactor: animation,
      axisAlignment: -1,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: FadeTransition(
          opacity: animation,
          child: Padding(
            padding: EdgeInsets.only(bottom: isLastItem ? 32 : 0),
            child: SourceItemTile(item),
          ),
        ),
      ),
    );
  }

  Future<List<SourceItem>> getSourceItems(Source source) async {
    final tappedUrls = prefs.getStringList(prefsKeyTappedArticleUrls) ?? [];

    final response = await get(source.feedUrl);
    var body;
    var parseFormat;
    List<SourceItem> sourceItems = [];
    try {
      body = RssFeed.parse(response.body);
      parseFormat = "E, dd MMM yyyy HH:mm:ss zzz";
      body.items.forEach((item) {
        final pubDate = DateFormat(parseFormat).parse(item.pubDate);
        sourceItems
            .add(SourceItem(fixSpecialCharacters(item.title), item.link, pubDate, tappedUrls.contains(item.link)));
      });
    } on ArgumentError {
      body = AtomFeed.parse(response.body);
      parseFormat = 'yyyy-MM-ddTHH:mm:ssZ';
      body.items.forEach((item) {
        final pubDate = DateFormat(parseFormat).parse(item.updated);
        final _url = item.links.first.href;
        sourceItems.add(SourceItem(fixSpecialCharacters(item.title), _url, pubDate, tappedUrls.contains(_url)));
      });
    }
    source.items.addAll(sourceItems);
    return sourceItems;
//    for testing offline
//    source.items.add(SourceItem('title', 'url', DateTime.now(), false));
//    return [SourceItem('title', 'url', DateTime.now(), false)];
  }

  clearOldRememberedUrls() async {
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
    prefs.getKeys().forEach((key) {
      final _date = getDateOrNullFromString(key);
      if (_date != null && today.difference(_date).inDays > 30) {
        prefs.remove(key);
      }
    });
  }

  removeOldSourceData() {
    final now = DateTime.now();
    sources.forEach((final Source source) {
      final isSourceOutOfDate = source.whenFetched != null && now.difference(source.whenFetched).inMinutes > 60;
      if (isSourceOutOfDate) {
        source.reset();
      }
    });
  }
}
