import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:southpaw/info_screen.dart';
import 'package:southpaw/source.dart';
import 'package:webfeed/webfeed.dart';

import 'constants.dart';
import 'globals.dart';

class SettingsScreen extends StatefulWidget {
  final shouldExpandAddSourceSection;

  SettingsScreen({this.shouldExpandAddSourceSection = false});

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool isFormVisible = false;
  bool areSourcesVisible = false;
  bool isOpenSourceExplanationVisible = false;
  Set<int> indexesBeingEdited = Set();
  double opacityOfForm = 0;
  double opacityOfSources = 0;
  double opacityOfFoss = 0;

  @override
  void initState() {
    isFormVisible = widget.shouldExpandAddSourceSection;
    opacityOfForm = widget.shouldExpandAddSourceSection ? 1 : 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bodies = [
      AnimatedOpacity(
        opacity: opacityOfForm,
        curve: Curves.easeIn,
        duration: Duration(milliseconds: isFormVisible ? 500 : 0),
        child: Visibility(
          visible: isFormVisible,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
            child: FormAddSource(_setState),
          ),
        ),
      ),
      AnimatedOpacity(
        opacity: opacityOfSources,
        curve: Curves.easeIn,
        duration: Duration(milliseconds: areSourcesVisible ? 500 : 0),
        child: Visibility(
          visible: areSourcesVisible,
          child: ListView.separated(
            primary: false,
            itemCount: sources.length,
            padding: EdgeInsets.only(bottom: 36),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final _sourceFormKey = GlobalKey<FormState>();
              final editSourceTitleController = TextEditingController();
              final source = sources[index];
              const imageSize = 36.0;
              final isEditing = indexesBeingEdited.contains(index);
              return Column(
                children: <Widget>[
                  ListTile(
                    leading: Image.network(
                      source.imageUrl,
                      width: imageSize,
                      height: imageSize,
                      scale: 0.8,
                      errorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                        return ConstrainedBox(
                          constraints: BoxConstraints(maxWidth: imageSize, maxHeight: imageSize),
                          child: Center(
                            child: Text(
                              source.initials,
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ),
                        );
                      },
                    ),
                    title: Text(source.title),
                    subtitle: Text(source.baseUrl),
                    trailing: SizedBox(
                      width: isEditing ? 140 : 64,
                      child: Row(
                        children: <Widget>[
                          Spacer(),
                          Visibility(
                            visible: isEditing,
                            child: RaisedButton(
                              child: Text('remove'),
                              color: Colors.deepOrange.shade900,
                              textColor: Colors.white,
                              onPressed: () {
                                setState(() {
                                  indexesBeingEdited.remove(index);
                                  final removedSource = sources[index];
                                  final removedSourceIndex = index;
                                  sources.removeAt(index);
                                  prefs.setStringList(
                                      prefsKeySources, sources.map((source) => source.toString()).toList());
                                  Scaffold.of(context).removeCurrentSnackBar();
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('Removed "${source.title}"'),
                                      action: SnackBarAction(
                                        label: 'undo',
                                        textColor: Theme.of(context).primaryColorLight,
                                        onPressed: () => setState(() {
                                          indexesBeingEdited.add(index);
                                          sources.insert(removedSourceIndex, removedSource);
                                          prefs.setStringList(
                                              prefsKeySources, sources.map((source) => source.toString()).toList());
                                        }),
                                      ),
                                      duration: Duration(seconds: 6),
                                    ),
                                  );
                                });
                              },
                            ),
                          ),
                          IconButton(
                            icon: Icon(isEditing ? Icons.arrow_drop_up : Icons.edit),
                            onPressed: () => setState(() {
                              if (isEditing) {
                                indexesBeingEdited.remove(index);
                              } else {
                                indexesBeingEdited.add(index);
                              }
                            }),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: isEditing,
                    child: Form(
                      key: _sourceFormKey,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(32, 0, 32, 16),
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              flex: 4,
                              child: TextFormField(
                                onTap: () => editSourceTitleController.text = source.title,
                                controller: editSourceTitleController,
                                decoration: InputDecoration(
                                  hintText: 'Acme News',
                                  labelText: 'new title',
                                ),
                                keyboardType: TextInputType.text,
                                validator: (title) {
                                  return title.trim().isEmpty ? 'please enter a title' : null;
                                },
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.done),
                              onPressed: () {
                                if (_sourceFormKey.currentState.validate()) {
                                  setState(() {
                                    source.title = editSourceTitleController.text;
                                    final sourcesAsStrings = sources.map((source) => source.toString()).toList();
                                    prefs.setStringList(prefsKeySources, sourcesAsStrings);
                                  });
                                }
                              },
                            ),
                            Spacer(),
                            Column(
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(Icons.keyboard_arrow_up),
                                  onPressed: index == 0
                                      ? null
                                      : () => setState(() {
                                            final newIndex = index - 1;
                                            indexesBeingEdited.remove(index);
                                            indexesBeingEdited.add(newIndex);
                                            sources.removeAt(index);
                                            sources.insert(newIndex, source);
                                            setSources();
                                          }),
                                ),
                                IconButton(
                                  icon: Icon(Icons.keyboard_arrow_down),
                                  onPressed: index == sources.length - 1
                                      ? null
                                      : () => setState(() {
                                            final newIndex = index + 1;
                                            indexesBeingEdited.remove(index);
                                            indexesBeingEdited.add(newIndex);
                                            sources.removeAt(index);
                                            sources.insert(newIndex, source);
                                            setSources();
                                          }),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
            separatorBuilder: (context, index) => Divider(),
          ),
        ),
      ),
      AnimatedOpacity(
        opacity: opacityOfFoss,
        curve: Curves.easeIn,
        duration: Duration(milliseconds: isOpenSourceExplanationVisible ? 500 : 0),
        child: Visibility(
          visible: isOpenSourceExplanationVisible,
          child: Padding(
            padding: EdgeInsets.fromLTRB(32, 0, 32, 16),
            child: Column(
              children: <Widget>[
                Text(
                  "We have no interest in knowing what your sources are.  We don't collect your data, and we never will."
                  "\n\n"
                  "This list of sources is stored only on your device.  This data doesn't get sent anywhere, and you can delete it anytime."
                  "\n\n"
                  "Your privacy matters to us, as does your trust.  That's why our code is open-source:",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                Row(
                  children: <Widget>[
                    CupertinoButton(
                      child: Text(
                        neverAdsUrl,
                        style: Theme.of(context).textTheme.subtitle1.copyWith(color: Theme.of(context).buttonColor),
                      ),
                      padding: EdgeInsets.symmetric(vertical: 16),
                      onPressed: () {
                        try {
                          openWebpage('https://$neverAdsUrl');
                        } catch (e) {
                          Scaffold.of(context).removeCurrentSnackBar();
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text('Unable to open "$neverAdsUrl".  Please check your network connection.'),
                            ),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    ];
    final headers = [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: <Widget>[
            CupertinoButton(
              child: Row(
                children: <Widget>[
                  Text('add a source', style: Theme.of(context).textTheme.headline5),
                  SizedBox(width: 4),
                  Icon(
                    isFormVisible ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                    color: Theme.of(context).buttonColor,
                  ),
                ],
              ),
              onPressed: () {
                setState(() {
                  isFormVisible = !isFormVisible;
                  opacityOfForm = isFormVisible ? 1 : 0;
                });
              },
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: Row(
          children: <Widget>[
            CupertinoButton(
              child: Row(
                children: <Widget>[
                  Text('my sources', style: Theme.of(context).textTheme.headline5),
                  SizedBox(width: 4),
                  Icon(
                    areSourcesVisible ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                    color: Theme.of(context).buttonColor,
                  ),
                ],
              ),
              onPressed: () {
                setState(() {
                  areSourcesVisible = !areSourcesVisible;
                  opacityOfSources = areSourcesVisible ? 1 : 0;
                });
              },
            ),
            Spacer(),
            Visibility(
              visible: areSourcesVisible,
              child: Builder(builder: (BuildContext context) {
                return IconButton(
                  icon: Icon(Icons.delete_sweep),
                  onPressed: () {
                    Scaffold.of(context).removeCurrentSnackBar();
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Would you like to permanently remove all sources?'),
                        action: SnackBarAction(
                          label: 'delete all',
                          textColor: Theme.of(context).primaryColorLight,
                          onPressed: () {
                            setState(() {
                              final List<Source> deletedSources = [];
                              deletedSources.addAll(sources);
                              sources.clear();
                              setSources();
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content: Text('Last chance…'),
                                  action: SnackBarAction(
                                    label: 'undo',
                                    textColor: Theme.of(context).primaryColorLight,
                                    onPressed: () {
                                      setState(() {
                                        sources = deletedSources;
                                        setSources();
                                      });
                                    },
                                  ),
                                  duration: Duration(seconds: 10),
                                ),
                              );
                            });
                          },
                        ),
                      ),
                    );
                  },
                );
              }),
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: <Widget>[
            CupertinoButton(
              child: Row(
                children: <Widget>[
                  Text(
                    'free and open-source',
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  SizedBox(width: 4),
                  Icon(
                    isOpenSourceExplanationVisible ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                    color: Theme.of(context).buttonColor,
                  ),
                ],
              ),
              onPressed: () {
                setState(() {
                  isOpenSourceExplanationVisible = !isOpenSourceExplanationVisible;
                  opacityOfFoss = isOpenSourceExplanationVisible ? 1 : 0;
                });
              },
            ),
          ],
        ),
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text('settings'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.info_outline),
            onPressed: () async {
              await Navigator.of(context).push(MaterialPageRoute(builder: (context) => InfoScreen()));
              setState(() {});
            },
          ),
        ],
      ),
      body: ListView(
          padding: EdgeInsets.only(top: 24, bottom: 72),
          children: <Widget>[
            headers[0],
            bodies[0],
            headers[1],
            bodies[1],
            headers[2],
            bodies[2],
          ],
        ),
    );
  }

  setSources() async {
    final prefsAsStrings = sources.map((source) => source.toString()).toList();
    prefs.setStringList(prefsKeySources, prefsAsStrings);
  }

  _setState() {
    setState(() {});
  }
}

class FormAddSource extends StatefulWidget {
  final callback;

  FormAddSource(this.callback);

  @override
  _FormAddSourceState createState() => _FormAddSourceState();
}

class _FormAddSourceState extends State<FormAddSource> {
  final _formKey = GlobalKey<FormState>();
  final addSourceTitleController = TextEditingController();
  final addSourceUrlController = TextEditingController();
  final delay = Future.delayed(Duration(milliseconds: 50));
  String title;
  String url;

  @override
  Widget build(BuildContext context) {
    const urlPrefix = 'https://';

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text(
              'Enter a URL, and we\'ll check to see if they have an RSS-feed.\n\n'
              'You can email us if you have trouble. (never.ads.info@gmail.com)',
              textAlign: TextAlign.start,
            ),
          ),
          TextFormField(
            controller: addSourceUrlController,
            decoration: InputDecoration(
              hintText: 'example.com/feed',
              labelText: 'URL',
              prefixText: urlPrefix,
              prefixStyle: Theme.of(context).textTheme.caption,
            ),
            keyboardType: TextInputType.url,
            validator: (url) {
              return url.trim().isEmpty ? 'please enter a URL' : null;
            },
          ),
          SizedBox(height: 24),
          TextFormField(
            controller: addSourceTitleController,
            decoration: InputDecoration(
              hintText: 'Acme News',
              labelText: 'source name',
            ),
            keyboardType: TextInputType.text,
            validator: (title) {
              return title.trim().isEmpty ? 'please enter a title' : null;
            },
          ),
          SizedBox(height: 24),
          RaisedButton(
            textColor: Theme.of(context).primaryColorLight,
            onPressed: () async {
              if (_formKey.currentState.validate()) {
                var title = addSourceTitleController.text.trim();
                var url = addSourceUrlController.text.trim();

                if (url.startsWith('www.')) {
                  url.replaceFirst('www.', '');
                }

                if (sources.any((source) => source.baseUrl == url || source.feedUrl == url)) {
                  _showSnackBar('Duplicate: "$url" is already in your list of sources.');
                } else {
                  Scaffold.of(context).removeCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      duration: Duration(seconds: 10),
                      content: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 24,
                            height: 24,
                            child: CircularProgressIndicator(strokeWidth: 3),
                          ),
                          SizedBox(width: 12),
                          Text('checking url…'),
                        ],
                      ),
                    ),
                  );
                  final rssFeedUrls = [
                    url,
                    '$url/feed/',
                    '${url}feed/',
                    '$url/rss.xml',
                  ];
                  final generalErrorMessage =
                      'Cannot connect to "$url".  Please check your network connection, and check the url for typos.';
                  Response response;
                  for (final rssFeedUrl in rssFeedUrls) {
                    try {
                      response = await get('$urlPrefix$rssFeedUrl');
                      if (response.statusCode == 404) {
                        _showSnackBar('Sorry, this website doesn\'t have an RSS-feed at "$rssFeedUrl".');
                      } else if (response != null) {
                        var body;
                        try {
                          body = RssFeed.parse(response.body);
                        } on ArgumentError {
                          body = AtomFeed.parse(response.body);
                        } catch (e) {
                          if (rssFeedUrl == rssFeedUrls.last) {
                            _showSnackBar('Error when trying to parse this RSS-feed.  Please check the url for typos.');
                          } else {
                            await delay;
                            continue;
                          }
                        }
                        if (body?.items == null) {
                          _showSnackBar('There is no data from "$rssFeedUrl".');
                        } else {
                          final n = body.items.length ?? 0;
                          _showSnackBar(
                            'Success!  Found $n item${n == 1 ? '' : 's'} in this source\'s RSS-feed.\n'
                            'Adding to sources-list…',
                          );
                          if (rssFeedUrl == rssFeedUrls.last) {
                            sources.add(Source(title, url));
                          } else {
                            sources.add(Source(title, url.split('/').first, rssFeedUrl));
                          }
                          widget.callback();
                          final sourcesAsStrings = sources.map((source) => source.toString()).toList();
                          prefs.setStringList(prefsKeySources, sourcesAsStrings);
                          break;
                        }
                      } else {
                        _showSnackBar(generalErrorMessage);
                      }
                    } catch (e) {
                      if (rssFeedUrl == rssFeedUrls.last) {
                        _showSnackBar(generalErrorMessage);
                      } else {
                        await delay;
                        continue;
                      }
                    }
                  }
                }
              }
            },
            child: Text('submit'),
          ),
        ],
      ),
    );
  }

  _showSnackBar(String text) {
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
      ),
    );
  }
}
