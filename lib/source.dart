import 'package:southpaw/constants.dart';

import 'globals.dart';

class Source {
  String title;
  final String baseUrl;
  DateTime whenFetched;
  List<SourceItem> items = [];
  Exception error;
  String _feedUrl;
  bool shouldDisplayItems = false;

  Source(this.title, this.baseUrl, [this._feedUrl]);

  factory Source.fromString(String sourceAsString) {
    try {
      final split = sourceAsString.split(sep);
      if (split.length == 2) {
        return Source(split.first, split.last);
      } else {
        return Source(split.first, split[1], split.last);
      }
    } catch (e) {
      return null;
    }
  }

  String get feedUrl {
    if (_feedUrl == null) {
      return 'https://$baseUrl/feed/';
    } else {
      return 'https://$_feedUrl';
    }
  }

  String get imageUrl {
    return 'https://s2.googleusercontent.com/s2/favicons?domain=http://$baseUrl';
  }

  String get initials {
    var initials = '';
    title.split(' ').forEach((word) {
      initials += word[0];
    });
    return initials;
  }

  List<SourceItem> getItemsWithDate(DateTime date) {
    if (items == null) {
      return null;
    } else {
      return items.where((item) => item.wasPublishedOnSameDayAsDate(date)).toList();
    }
  }

  String toString() {
    if (_feedUrl == null) {
      return '$title$sep$baseUrl';
    } else {
      return '$title$sep$baseUrl$sep$_feedUrl';
    }
  }

  reset() {
    error = null;
    whenFetched = null;
    items.clear();
  }
}

// final List<Source> defaultSources = [
  // Source(
  //   'Caitlin Johnstone',
  //   'caitlinjohnstone.com',
  // ),
  // Source(
  //   'The Grayzone',
  //   'thegrayzone.com',
  // ),
  // Source(
  //   'Jacobin Magazine',
  //   'jacobinmag.com',
  // ),
  // Source(
  //   'Lee Camp',
  //   'leecamp.com',
  // ),
  // Source(
  //   'Shadowproof',
  //   'shadowproof.com',
  // ),
  // Source(
  //   'Liberation News',
  //   'liberationnews.org',
  // ),
  // Source(
  //   'Electronic Intifada',
  //   'electronicintifada.net',
  //   'electronicintifada.net/rss.xml',
  // ),
// ];

class SourceItem {
  final String title;
  final String url;
  final DateTime _date;
  bool isChecked;

  SourceItem(this.title, this.url, this._date, this.isChecked);

  factory SourceItem.fromString(String sourceItemAsString) {
    final split = sourceItemAsString.split(sep);
    return SourceItem(
      split.first,
      split[1],
      DateTime.fromMillisecondsSinceEpoch(int.parse(split.last)),
      false,
    );
  }

  DateTime get date {
    return DateTime(_date.year, _date.month, _date.day);
  }

  get isFavorite {
    List<String> favorites = prefs.getStringList(prefsKeyFavorites) ?? [];
    return favorites.contains(this.toString());
  }

  bool wasPublishedOnSameDayAsDate(DateTime _date) {
    return date.year == _date.year && date.month == _date.month && date.day == _date.day;
  }

  String toString() {
    return '$title$sep$url$sep${date.millisecondsSinceEpoch}';
  }

  toggleAsFavorite() {
    List<String> favorites = prefs.getStringList(prefsKeyFavorites) ?? [];
    if (favorites.contains(this.toString())) {
      favorites.remove(this.toString());
    } else {
      favorites.add(this.toString());
    }
    prefs.setStringList(prefsKeyFavorites, favorites.toSet().toList());
  }

  String get imageUrl {
    return 'https://s2.googleusercontent.com/s2/favicons?domain=$url';
  }
}
