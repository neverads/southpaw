import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'constants.dart';

class InfoScreen extends StatefulWidget {
  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  final listKey = GlobalKey<AnimatedListState>();
  List<Widget> _listItems = [];

  bool isSectionWhatItDoVisible = false;
  bool isSectionHowItDoVisible = false;
  bool isSectionSourceVisible = false;
  bool isSectionHelpVisible = false;
  bool isSectionFavoritesVisible = false;

  @override
  Widget build(BuildContext context) {
    final bodies = [
      Text(
        'This app gets information from websites and shows it to you.  '
        'Typically this would be in the form of news articles.\n\n'
        'Other similar "news feeds" and social networks apply algorithms to manipulate what you see - we don\'t.\n\n',
        style: Theme.of(context).textTheme.subtitle1,
      ),
      Text(
        'Most news websites publish their recent articles in a special format called a "feed," '
        'so apps like this can easily get links to news articles.\n\n'
        'We check a website\'s feed and show you the headlines & links.\n\n',
        style: Theme.of(context).textTheme.subtitle1,
      ),
      Text(
        'A "source" is our term for a website from which we can get news articles.\n\n'
        'We include a few by default, because we want users to see how the app works right when they install it.  '
        'Maybe you\'ll like them.  If not, you can delete them on the settings screen.\n\n',
        style: Theme.of(context).textTheme.subtitle1,
      ),
      Column(
        children: [
          Text(
            'News websites typically have their "feeds" in the same place: "https://example.com/feed/".  '
            'If they publish it elsewhere, our code won\'t know where to look.\n\n'
            'However, if you know exactly where their "feed" is, you can use the "advanced" option to enter the exact URL.\n\n'
            'If you have any trouble adding a "source," feel free to email us for help.',
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Builder(
              builder: (context) {
                return CupertinoButton(
                  padding: EdgeInsets.zero,
                  child: Text(
                    neverAdsEmail,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(color: Theme.of(context).buttonColor),
                  ),
                  onPressed: () {
                    Clipboard.setData(ClipboardData(text: neverAdsEmail));
                    Scaffold.of(context).removeCurrentSnackBar();
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Email copied!'),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
      Text(
        '"Favorites" are just articles which you\'d like to keep around for future reference.\n\n'
        'On the main screen, tap the ★ button to add the article to your list of favorites.  Tap again to remove.\n\n'
        'To view your favorites:\n'
        '1. Go to the main screen.\n'
        '2. Open the menu top-right corner.\n'
        '3. Tap "favorites."\n'
        'Your favorites will be displayed there, sorted by date.\n',
        style: Theme.of(context).textTheme.subtitle1,
      ),
    ];

    final headers = [
      Align(
        alignment: Alignment.centerLeft,
        child: CupertinoButton(
          padding: EdgeInsets.symmetric(vertical: 24),
          child: Text(
            'What does this app do?',
            style: Theme.of(context).textTheme.headline5,
          ),
          onPressed: () => setState(() {
            isSectionWhatItDoVisible = !isSectionWhatItDoVisible;
            const itemIndex = 1;
            final body = bodies[0];
            if (isSectionWhatItDoVisible) {
              listKey.currentState.insertItem(itemIndex);
              _listItems.insert(itemIndex, body);
            } else {
              listKey.currentState.removeItem(
                itemIndex,
                (_, animation) => SizeTransition(
                  sizeFactor: CurvedAnimation(parent: animation, curve: Interval(0.0, 1.0, curve: Curves.easeIn)),
                  axisAlignment: -1.0,
                  child: body,
                ),
              );
              _listItems.removeAt(itemIndex);
            }
          }),
        ),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: CupertinoButton(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Text(
            'How does this app get info from websites?',
            style: Theme.of(context).textTheme.headline5,
          ),
          onPressed: () => setState(() {
            final itemIndex = 2 + (isSectionWhatItDoVisible ? 1 : 0);
            final body = bodies[1];

            isSectionHowItDoVisible = !isSectionHowItDoVisible;
            if (isSectionHowItDoVisible) {
              listKey.currentState.insertItem(itemIndex);
              _listItems.insert(itemIndex, body);
            } else {
              listKey.currentState.removeItem(
                itemIndex,
                (_, animation) => SizeTransition(
                  sizeFactor: CurvedAnimation(parent: animation, curve: Interval(0.0, 1.0, curve: Curves.easeIn)),
                  axisAlignment: -1.0,
                  child: body,
                ),
              );
              _listItems.removeAt(itemIndex);
            }
          }),
        ),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: CupertinoButton(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Text(
            'What is a "source?"',
            style: Theme.of(context).textTheme.headline5,
          ),
          onPressed: () => setState(() {
            var itemIndex = 3 + (isSectionWhatItDoVisible ? 1 : 0) + (isSectionHowItDoVisible ? 1 : 0);
            final body = bodies[2];

            isSectionSourceVisible = !isSectionSourceVisible;
            if (isSectionSourceVisible) {
              listKey.currentState.insertItem(itemIndex);
              _listItems.insert(itemIndex, body);
            } else {
              listKey.currentState.removeItem(
                itemIndex,
                (_, animation) => SizeTransition(
                  sizeFactor: CurvedAnimation(parent: animation, curve: Interval(0.0, 1.0, curve: Curves.easeIn)),
                  axisAlignment: -1.0,
                  child: body,
                ),
              );
              _listItems.removeAt(itemIndex);
            }
          }),
        ),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: CupertinoButton(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Text(
            'What if the app can\'t find the "source" I want?',
            style: Theme.of(context).textTheme.headline5,
          ),
          onPressed: () => setState(() {
            var itemIndex = 4 +
                (isSectionWhatItDoVisible ? 1 : 0) +
                (isSectionHowItDoVisible ? 1 : 0) +
                (isSectionSourceVisible ? 1 : 0);
            final body = bodies[3];

            isSectionHelpVisible = !isSectionHelpVisible;
            if (isSectionHelpVisible) {
              _listItems.insert(itemIndex, body);
              listKey.currentState.insertItem(itemIndex);
            } else {
              listKey.currentState.removeItem(
                itemIndex,
                (_, animation) => SizeTransition(
                  sizeFactor: CurvedAnimation(parent: animation, curve: Interval(0.0, 1.0, curve: Curves.easeIn)),
                  axisAlignment: -1.0,
                  child: body,
                ),
              );
              _listItems.removeAt(itemIndex);
            }
          }),
        ),
      ),
      Align(
        alignment: Alignment.centerLeft,
        child: CupertinoButton(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Text(
            'What are "favorites" and how do they work?',
            style: Theme.of(context).textTheme.headline5,
          ),
          onPressed: () => setState(() {
            var itemIndex = 5 +
                (isSectionWhatItDoVisible ? 1 : 0) +
                (isSectionHowItDoVisible ? 1 : 0) +
                (isSectionSourceVisible ? 1 : 0) +
                (isSectionHelpVisible ? 1 : 0);
            final body = bodies[4];

            isSectionFavoritesVisible = !isSectionFavoritesVisible;
            if (isSectionFavoritesVisible) {
              _listItems.insert(itemIndex, body);
              listKey.currentState.insertItem(itemIndex);
            } else {
              listKey.currentState.removeItem(
                itemIndex,
                (_, animation) => SizeTransition(
                  sizeFactor: CurvedAnimation(parent: animation, curve: Interval(0.0, 1.0, curve: Curves.easeIn)),
                  axisAlignment: -1.0,
                  child: body,
                ),
              );
              _listItems.removeAt(itemIndex);
            }
          }),
        ),
      ),
    ];

    _listItems.addAll(headers);

    return Scaffold(
      appBar: AppBar(
        title: Text(infoScreenTitle),
      ),
      body: AnimatedList(
        key: listKey,
        initialItemCount: headers.length,
        padding: EdgeInsets.fromLTRB(24, 16, 24, 64),
        itemBuilder: (BuildContext context, int index, Animation<double> animation) {
          return SizeTransition(
            sizeFactor: CurvedAnimation(parent: animation, curve: Interval(0.0, 1.0, curve: Curves.easeIn)),
            axisAlignment: -1.0,
            child: _listItems[index],
          );
        },
      ),
    );
  }
}
