import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:southpaw/source.dart';
import 'package:url_launcher/url_launcher.dart';

import 'constants.dart';

SharedPreferences prefs;

List<Source> sources = [];

String getDateAsString(DateTime date) {
  final now = DateTime.now();
  final today = DateTime(now.year, now.month, now.day);
  if (today.difference(date) == Duration(days: 0)) {
    return todayString;
  } else if (today.difference(date) == Duration(days: 1)) {
    return yesterdayString;
  } else {
    return DateFormat(parseFormat).format(date);
  }
}

DateTime getDateOrNullFromString(String dateAsString) {
  try {
    return DateFormat(parseFormat).parse(dateAsString);
  } catch (e) {
    return null;
  }
}

openWebpage(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  }
}

String fixSpecialCharacters(String text) {
  String fixed = text.trim();
  fixed = fixed.replaceAll('â', "'");
  fixed = fixed.replaceAll('â', "'");
  fixed = fixed.replaceAll('â', '"');
  fixed = fixed.replaceAll('â', '"');
  fixed = fixed.replaceAll('Ã', 'Á');
  fixed = fixed.replaceAll('â', '—');
  fixed = fixed.replaceAll('&#8217;', '\'');
  fixed = fixed.replaceAll('&#8220;', '"');
  fixed = fixed.replaceAll('&#8221;', '"');
  fixed = fixed.replaceAll('<cite>', '"');
  fixed = fixed.replaceAll('</cite>', '"');
  fixed = fixed.replaceAll('Ã©', 'é');
  fixed = fixed.replaceAll('â', '\'');
  return fixed;
}
